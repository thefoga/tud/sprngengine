#include <iostream>
#include <map>
#include <random>

#include "random/SprngEngine.hpp"

// available SPRNG engines
using Default_SprngEngine = SprngEngine<std::uint_fast32_t, SPRNG_DEFAULT>;
using LFG_SprngEngine = SprngEngine<std::uint_fast32_t, 0>;
using LCG_SprngEngine = SprngEngine<std::uint_fast32_t, 1>;
using LCG64_SprngEngine = SprngEngine<std::uint_fast32_t, 2>;
using CMRG_SprngEngine = SprngEngine<std::uint_fast32_t, 3>;
using MLFG_SprngEngine = SprngEngine<std::uint_fast32_t, 4>;
using PMLCG_SprngEngine = SprngEngine<std::uint_fast32_t, 5>;

void printLine(const int w = 80, const char l = '=') {
  for (auto i = 0; i < w; ++i) {
    std::cout << l;
  }
  std::cout << std::endl;
}

void printTitle(std::string title) {
  std::cout << std::endl;
  printLine();
  std::cout << title << std::endl;
  printLine();
  std::cout << std::endl;
}

void testNormal(std::random_device& rd, const double mu, const double std) {
  printTitle("testing normal distribution");

  LFG_SprngEngine gen(rd());
  std::cout << gen << std::endl;

  std::normal_distribution<> d{mu, std};
  std::map<int, int> hist{};

  for (int i = 0; i < 1e4; ++i) {
    ++hist[std::round(d(gen))];
  }

  for (auto p : hist) {
    std::cout << p.first << ' ' << std::string(p.second / 200, '*') << '\n';
  }
}

void testPI(std::random_device& rd,
            const unsigned int n,
            std::string stateFile) {
  printTitle("estimating PI");
  SprngEngine<std::uint_fast32_t, 1>* gen;
  std::uniform_real_distribution<double> dist(0, 1);  // up-right quadrant

  // reload state
  unsigned int nOld = 0, nInside = 0, sizeOld = 0;
  char buffer[MAX_PACKED_LENGTH];
  FILE* fp = fopen(stateFile.c_str(), "r");
  if (fp) {
    fread(&nInside, 1, sizeof(int), fp);
    fread(&nOld, 1, sizeof(int), fp);
    fread(&sizeOld, 1, sizeof(int), fp);
    fread(buffer, 1, sizeOld, fp);
    fclose(fp);

    gen = new LCG_SprngEngine(buffer);
    std::cout << "Reload complete" << std::endl;
  } else {
    std::cerr << "Cannot read state. Creating new generator" << std::endl;
    gen = new LCG_SprngEngine(rd());
  }

  // show generator
  std::cout << *gen << std::endl;

  // generate randoms
  for (auto i = 0; i < n; ++i) {
    pointnumber x = dist(*gen);
    pointnumber y = dist(*gen);

    if ((pow(x, 2) + pow(y, 2)) < 1) {
      nInside += 1;
    }
  }
  nOld += n;  // update counts

  // save state
  fp = fopen(stateFile.c_str(), "w");
  char* bytes;
  if (fp) {
    fwrite(&nInside, 1, sizeof(int), fp);
    fwrite(&nOld, 1, sizeof(int), fp);

    const int size = gen->pack(&bytes);
    fwrite(&size, 1, sizeof(int), fp);
    fwrite(bytes, 1, size, fp);

    fclose(fp);
    free(bytes);
  } else {
    std::cerr << "Cannot save state" << std::endl;
  }
  delete gen;

  // show results
  pointnumber estimate = 4.0 * nInside / nOld;
  pointnumber error = abs(estimate - M_PI);
  pointnumber p = M_PI / 4.0;
  pointnumber stdError = 4.0 * sqrt(p * (1 - p) / nOld);
  std::cout << "pi ~ " << estimate << " +- " << stdError << " using " << nOld
            << " samples" << std::endl;
  std::cout << "error ~ " << error << std::endl;

  // finalize
  std::cout << "state saved in '" << stateFile << "'" << std::endl;
}

#ifdef USE_MPI

#include "util/mpi_utils.hpp"

void testMPI(int argc, char* argv[], const int n) {
  MPI_Init(&argc, &argv);

  if (amIMaster()) {
    printTitle("testing MPI");
  }

  const int seed = Default_SprngEngine::makeSeed();  // should be the same
  LCG64_SprngEngine s(seed);
  std::cout << s << std::endl;

  std::uniform_real_distribution<double> dist(0, 1);  // up-right quadrant
  for (auto i = 0; i < n; ++i) {
    std::cout << "RN from MPI #" << getRank() << ": " << dist(s) << std::endl;
  }

  MPI_Finalize();
}

#endif

auto main(int argc, char* argv[]) -> int {
#ifdef USE_MPI
  testMPI(argc, argv, 10);
#else
  std::random_device rd;
  testNormal(rd, 5, 2);
  testPI(rd, 1e5, "state.sprng");
#endif

  return 0;
}
