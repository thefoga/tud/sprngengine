include openfpm.defs
include sprng.defs
include mpi.defs
include clang.defs
include tests.defs

# files and folders
cwd						:= $(shell pwd)
build_folder	:= build
build_out			:= $(build_folder)/main.out

# compile
compiler	    := mpicxx
compile_flags := -g -c -std=c++11
src_folder		:= src
includes      := $(sprng_inc) $(openfpm_inc) -I$(src_folder)/

# link
linker        := mpicxx
link_flags    := $(sprng_link) $(openfpm_link)
libs          := $(sprng_libs) $(openfpm_libs)

# debug
debugger      := xterm -hold -e gdb
debug_flags   := -ex run --args

# cleaning

%:
ifeq ($(USE_MPI), YES)
	$(eval compile_flags := $(compile_flags) $(mpi_flags))
endif

ifeq ($(TEST), YES)
	$(eval compile_flags := $(compile_flags) -DBOOST_TEST_DYN_LINK)
endif

	$(compiler) $(compile_flags) $(includes) -o $(build_folder)/$@.o $(src_folder)/$@.cpp

clean:
	rm -rf $(build_folder)

compile:
	mkdir -p $(build_folder)
	$(MAKE) main

link:
	$(linker) -o $(build_out) $(build_folder)/*.o $(link_flags) $(libs)

build:
	$(MAKE) compile
	$(MAKE) link

run:
ifeq ($(USE_MPI), YES)
	mpirun -np $(mpi_procs) ./$(build_out)
else
	./$(build_out)
endif

rerun:
	$(MAKE) clean
	$(MAKE) build
	$(MAKE) run

debug:
	$(debugger) $(debug_flags) ./$(build_out)

redebug:
	$(MAKE) build
	$(MAKE) debug
